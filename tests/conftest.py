from unittest.mock import MagicMock

import pytest

from app.src.main import app, rds_session

mock_session = MagicMock()


def overrride_get_session():
    try:
        yield mock_session
    finally:
        pass


app.dependency_overrides[rds_session] = overrride_get_session


@pytest.fixture
def mock_db_session():
    return mock_session
