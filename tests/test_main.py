import pytest
from fastapi.testclient import TestClient

from app.src.main import app

client = TestClient(app)


@pytest.mark.skip(reason="tests should be adopted to the new access functionality")
def test_get_projects_ls(mock_db_session):
    """
    Getting of the projects' list
    """
    response = client.get("/projects")
    assert response.status_code == 200


@pytest.mark.skip(reason="tests should be adopted to the new access functionality")
def test_create_proj(mock_db_session):
    """
    Creation of the project with full information
    """
    response = client.post(
        "/projects",
        json={
            "name": "1-st test project",
            "description": "1-st test project info",
            "created_by": "1-st user name",
        },
    )

    assert response.status_code == 201
    data = response.json()
    assert data["name"] == "1-st test project"
    assert data["description"] == "1-st test project info"
    assert data["created_by"] == "1-st user name"

    mock_db_session.add.assert_called()
    mock_db_session.commit.assert_called()


@pytest.mark.skip(reason="tests should be adopted to the new access functionality")
def test_create_proj_wo_descript(mock_db_session):
    """
    Creation of the project without description
    """
    response = client.post(
        "/projects", json={"name": "2-nd test project", "created_by": "2-nd user name"}
    )

    assert response.status_code == 201
    data = response.json()
    assert data["name"] == "2-nd test project"
    assert data["description"] == ""
    assert data["created_by"] == "2-nd user name"

    mock_db_session.add.assert_called()
    mock_db_session.commit.assert_called()


@pytest.mark.skip(reason="tests should be adopted to the new access functionality")
def test_create_proj_w_long_name(mock_db_session):
    """
    Creation of the project with excessively long name
    Length of the project name more than 100 char
    """
    response = client.post(
        "/projects",
        json={
            "name": "3-rd project with were long name that exceed 100 simbols \
#                 .............................................",
            "description": "3-rd test project info",
            "created_by": "3-rd user name",
        },
    )

    assert response.status_code == 422


@pytest.mark.skip(reason="tests should be adopted to the new access functionality")
def test_create_proj_wo_proj_name(mock_db_session):
    """
    Creation of the project without project name
    """
    response = client.post(
        "/projects",
        json={"description": "4-rd test project info", "created_by": "4-rd user name"},
    )

    assert response.status_code == 422


@pytest.mark.skip(reason="tests should be adopted to the new access functionality")
def test_create_proj_wo_author_name(mock_db_session):
    """
    Creation of the project without author name
    """
    response = client.post(
        "/projects",
        json={"name": "5-th test project", "description": "5-th test project info"},
    )

    assert response.status_code == 422
