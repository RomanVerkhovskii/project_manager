import boto3
import os
from PIL import Image

s3 = boto3.client("s3")


def resize_uploaded_logo(path: str, resized_path: str):
    """
    Resize an uploaded logo image.

    :path: str - Path to the original image file.
    :resized_path: str - Path where the resized image will be saved.
    """
    image = Image.open(path)
    resized_image = image.resize((400, 400))
    resized_image.save(resized_path)


def lambda_handler(event, context):
    """
    Lambda function to resize an uploaded logo image in an S3 bucket.

    :event: dict - A dictionary containing information about the event
    that triggered the Lambda function.
    :context: LambdaContext - An object containing runtime information
    for the Lambda function.
    """
    bucket_name = event["Records"][0]["s3"]["bucket"]["name"]
    key = event["Records"][0]["s3"]["object"]["key"]
    download_path = "/tmp/logo-file.png"
    upload_path = "/tmp/resized-logo-file.png"
    target_bucket_name = os.getenv("TARGET_BUCKET_NAME")
    try:
        s3.download_file(bucket_name, key, download_path)
        resize_uploaded_logo(download_path, upload_path)
        s3.upload_file(upload_path, target_bucket_name, key)

    except Exception as e:
        print(e)
        print(
            f"""Error getting object {key} from bucket {bucket_name}.
        Make sure they exist and your bucket is in the
        same region as this function."""
        )
        raise e
