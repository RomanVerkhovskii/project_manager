CREATE TABLE alembic_version (
    version_num VARCHAR(32) NOT NULL, 
    CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num)
);

CREATE TABLE users (
    u_id SERIAL NOT NULL, 
    login VARCHAR, 
    password VARCHAR, 
    PRIMARY KEY (u_id)
);

CREATE TABLE projects (
    p_id SERIAL NOT NULL, 
    name VARCHAR, 
    description TEXT, 
    created_at TIMESTAMP WITH TIME ZONE DEFAULT now(), 
    updated_at TIMESTAMP WITHOUT TIME ZONE, 
    created_by INTEGER, 
    updated_by INTEGER, 
    PRIMARY KEY (p_id), 
    FOREIGN KEY(created_by) REFERENCES users (u_id), 
    FOREIGN KEY(updated_by) REFERENCES users (u_id)
);

CREATE TABLE documents (
    id SERIAL NOT NULL, 
    link VARCHAR, 
    project_id INTEGER, 
    PRIMARY KEY (id), 
    FOREIGN KEY(project_id) REFERENCES projects (p_id)
);

CREATE TABLE logos (
    id SERIAL NOT NULL, 
    link VARCHAR, 
    project_id INTEGER, 
    PRIMARY KEY (id), 
    FOREIGN KEY(project_id) REFERENCES projects (p_id)
);

CREATE TABLE user_access (
    ua_id SERIAL NOT NULL, 
    user_id INTEGER, 
    project_id INTEGER, 
    role VARCHAR, 
    PRIMARY KEY (ua_id), 
    FOREIGN KEY(project_id) REFERENCES projects (p_id), 
    FOREIGN KEY(user_id) REFERENCES users (u_id)
);

INSERT INTO alembic_version (version_num) VALUES ('92104815ff10') RETURNING alembic_version.version_num;

