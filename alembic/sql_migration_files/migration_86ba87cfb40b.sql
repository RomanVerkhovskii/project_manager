ALTER TABLE documents ADD COLUMN path VARCHAR;

ALTER TABLE documents ADD COLUMN doc_name VARCHAR;

ALTER TABLE documents DROP COLUMN link;

ALTER TABLE logos ADD COLUMN path VARCHAR;

ALTER TABLE logos ADD COLUMN logo_file_name VARCHAR;

ALTER TABLE logos DROP COLUMN link;

UPDATE alembic_version SET version_num='86ba87cfb40b' WHERE alembic_version.version_num = '92104815ff10';
