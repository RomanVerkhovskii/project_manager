FROM python:3.10

WORKDIR /usr/src/app

COPY . .

RUN pip install pipenv && \
    pipenv install

CMD ["pipenv", "run", "alembic", "upgrade", "head"]
