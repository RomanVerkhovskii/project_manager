from typing import List

from fastapi import HTTPException, UploadFile, status
from magic import from_buffer

from ..models.alembic_models import Document
from .logger import logger


class DocumentServices:
    """
    Provides services related to document upload operations.
    """

    @classmethod
    def check_document_attachment(cls, document):
        """
        Checks if a document has been attached/uploaded.

        :document: The document file to be checked.
        """
        if not document.filename:
            logger.info(msg="File has not been uploaded")
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="File has not been uploaded",
            )

    @classmethod
    def check_doc_existence(cls, session, document_id: int):
        """
        Check if a document with the given ID exists in the database.

        :document_id: int - The ID of the document to check;
        :session: Session - The SQLAlchemy session object for database operations;
        :return: The document if it exists, otherwise raise an HTTPException.
        """
        project_doc = session.query(Document).filter_by(id=document_id).first()

        if not project_doc:
            logger.error(msg="There is no document with such ID.")
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="There is no document with such ID.",
            )
        return project_doc

    @classmethod
    def check_file_format(cls, file: UploadFile, allowed_format: List[str]):
        """
        Check if the uploaded file format is allowed.

        :file: UploadFile - The uploaded file object;
        :allowed_format: List[str] - A list of allowed MIME types;
        :returns: bool - True if the file format is allowed, False otherwise.
        """

        allowed = True

        file_content = file.file.read(1024)
        file.file.seek(0)
        file_mime_type = from_buffer(file_content, mime=True)

        if file_mime_type not in allowed_format:
            allowed = False

        return allowed
