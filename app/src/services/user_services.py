import hashlib

from fastapi import HTTPException, status

from ..models.alembic_models import User
from ..models.user import UserCreateIn, UserLoginIn
from .logger import logger


class UserServices:
    """
    Class providing functionality related with new user creation process.
    """

    @classmethod
    def check_password_match(cls, user_info: UserCreateIn):
        """
        Check if the password and repeat_password match.

        :user_info: UserCreateIn.
        """
        if user_info.password != user_info.repeat_password:
            logger.error(msg="Passwords do not match")
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Passwords do not match",
            )

    @classmethod
    def check_user_repetitiveness(cls, user_info: UserCreateIn, session):
        """
        Check if the user with the given login already exists in the users table
        of the projects_db and if yes raise the HTTP exception
        with status code 409 (Conflict).

        :session: Session - The RDS session;
        :user_info: UserCreateIn.
        """
        check_login_existence = (
            session.query(User.login).filter_by(login=user_info.login).first()
            is not None
        )
        if check_login_existence:
            logger.error(msg=f"The user with {user_info.login} already exists")
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail=f"The user with {user_info.login} already exists",
            )

    @classmethod
    def get_user(cls, session, user_login: str):
        """
        Check if the user with the given login exists in the users table
        of the projects_db and if no raise
        the HTTP exception with status code 404 (Not found).

        :user_login: str;
        :session: Session - The RDS session;
        :return: User.
        """
        user = session.query(User).filter_by(login=user_login).first()
        if not user:
            logger.error(msg=f"The user with {user_login} doesn't exist")
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"The user with login: {user_login} doesn't exist",
            )
        return user

    @classmethod
    def check_user_password(cls, user, user_info: UserLoginIn):
        """
        Check if the given password match with the password of the given user stored
        in the users table of the projects_db and if not raise the HTTP exception with
        status code 401 (Unauthorized):

        :user: User;
        :user_info: UserLoginIn.
        """
        provided_password = cls.hash_password(user_info.password)

        if provided_password != user.password:
            logger.error(msg="Wrong password")
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail="Wrong password"
            )

    @classmethod
    def hash_password(cls, password: str):
        """
        Hash given password and return it as a result of the function:

        :password: str;
        :return: str (hashed password).
        """
        password_bytes = password.encode("utf-8")
        hashed_bytes = hashlib.sha256(password_bytes)
        hashed_password = hashed_bytes.hexdigest()

        return hashed_password
