import os
from datetime import datetime, timedelta

import jwt
from dotenv import load_dotenv

load_dotenv()


class JWTServices:
    """
    Class providing functionality related with JWT encryption and verification.
    """

    @classmethod
    def encrypt_access_token(cls, login: str, expiration_time: int = None):
        """
        Encrypt JWT containing login and expiration_time and
        return it as a result of the function:

        :login: str;
        :expiration_time: timedelta - default expiration period - 10 minutes;
        :return: JWT.
        """
        if expiration_time:
            expiration_timedelta = timedelta(minutes=expiration_time)
            expire = datetime.now() + expiration_timedelta
        else:
            expire = datetime.now() + timedelta(minutes=10)
        jwt_content = {"login": login, "exp": expire}
        access_token = jwt.encode(
            jwt_content, os.getenv("JWT_SECRET"), algorithm=os.getenv("JWT_ALGORITHM")
        )
        return access_token

    @classmethod
    def decrypt_access_token(cls, access_token: str):
        """
        Decrypt JWT and return its content as a result of the function:

        :access_token: str;
        :return: dict.
        """
        jwt_content = jwt.decode(
            access_token[len("Bearer ") :],
            os.getenv("JWT_SECRET"),
            algorithms=[os.getenv("JWT_ALGORITHM")],
        )
        return jwt_content
