from fastapi import HTTPException, status

from ..models.alembic_models import Logo
from .logger import logger


class LogoServices:
    """
    Provides services related to logo upload operations.
    """

    @classmethod
    def check_logo_existence(cls, session, project_id: int):
        """
        Check if the project with given ID has logo.

        :project_id: int - The ID of the project to check;
        :session: Session - The SQLAlchemy session object for database operations;
        :return: The document if it exists, otherwise raise an HTTPException.
        """
        project_logo = session.query(Logo).filter_by(project_id=project_id).first()

        if not project_logo:
            logger.error(msg="The project with such ID does not have logo.")
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="The project with such ID does not have logo.",
            )
        return project_logo
