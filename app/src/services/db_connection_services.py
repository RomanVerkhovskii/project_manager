import os

import boto3
from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.engine import URL

load_dotenv()


class DBConnectionServices:
    """
    A utility class for managing database connections and operations.
    """

    def __init__(self):
        boto_session = boto3.Session(
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
            region_name=os.getenv("REGION"),
        )
        self.client = boto_session.client("rds")

    def get_rds_engine(self):
        """
        Creates and returns a SQLAlchemy engine connected to the RDS database.

        :return: A SQLAlchemy engine connected to the RDS database.
        """

        PASSWORD = self.client.generate_db_auth_token(
            DBHostname=os.getenv("AWS_DB_HOST_NAME"),
            Port=os.getenv("PORT"),
            DBUsername=os.getenv("USER_NAME"),
            Region=os.getenv("REGION"),
        )

        database_url = URL(
            drivername=os.getenv("DRIVER_NAME"),
            username=os.getenv("USER_NAME"),
            password=PASSWORD,
            port=os.getenv("PORT"),
            query={},
            host=os.getenv("AWS_DB_HOST_NAME"),
            database=os.getenv("DB_NAME"),
        )

        engine = create_engine(database_url)

        return engine

    def get_local_engine(self):
        """
        Create and return a SQLAlchemy engine connected to a local database.

        :returns: sqlalchemy.engine.base.Engine - A SQLAlchemy engine connected
        to the local database.
        """

        database_url = URL.create(
            drivername=os.getenv("DRIVER_NAME"),
            username=os.getenv("USER_NAME"),
            password=os.getenv("USER_PASSWORD"),
            host=os.getenv("DOCKER_DB_HOST_NAME"),
            database=os.getenv("DB_NAME"),
            port=os.getenv("PORT"),
        )

        engine = create_engine(database_url)

        return engine
