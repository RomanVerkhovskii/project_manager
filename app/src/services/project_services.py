from fastapi import HTTPException, status
from pydantic import EmailStr
from sqlalchemy.orm import sessionmaker

from ..models.alembic_models import Project, User, UserAccess
from .db_connection_services import DBConnectionServices
from .logger import logger


class ProjectServices:
    """
    Class providing functionality related with projects.
    """

    db_connection = DBConnectionServices()
    Session = sessionmaker(bind=db_connection.get_rds_engine())
    session = Session()

    @classmethod
    def get_project(cls, project_id: int):
        """
        Check the existence of the project with given project id.

        :session: Session - The SQLAlchemy session object for database operations;
        :project_id: int.
        """
        project = cls.session.query(Project).filter_by(p_id=project_id).first()
        exists = project is not None

        if not exists:
            logger.error(msg="There is no project with such ID")
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="There is no project with such ID",
            )

        return project

    @classmethod
    def compare_owner_invited_user(cls, owner: User, invited_user: EmailStr):
        """
        Compare invited user login and the owner login and if they match raise the error.

        :owner: str (the login of the project's owner);
        :invited_user: str (the login of the invited user)
        """
        if owner.login == invited_user:
            logger.warning(
                msg=f"The invited user {invited_user} is already \
the admin of this project"
            )
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"The invited user {invited_user} is already \
the admin of this project",
            )

    @classmethod
    def check_user_authority(cls, user: User, project_id: int, authorized_roles: list):
        """
        Check if the given user possesses proper permissions (proper role)
        in the project with the given id.

        :user: User;
        :project_id: int;
        :authorized_roles: list (the list of roles which are authorized)
        """
        users_with_access = []
        for considered_role in authorized_roles:
            users_with_access += (
                cls.session.query(UserAccess)
                .filter_by(project_id=project_id, role=considered_role)
                .all()
            )

        access = False

        for user_with_access in users_with_access:
            if user_with_access.user_id == user.u_id:
                access = True

        if not access:
            logger.error(msg="Access denied. User is not authorized for such action.")
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Access denied. User is not authorized for such action.",
            )
