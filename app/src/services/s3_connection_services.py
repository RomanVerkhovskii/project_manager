import os

import boto3
from dotenv import load_dotenv

load_dotenv()


class S3ConnectionServices:
    """
    A class for managing connections to Amazon S3.
    """

    @classmethod
    def get_s3_client(cls):
        """
        Get an instance of the Amazon S3 client.

        :returns: botocore.client.S3 - An instance of the Amazon S3 client.
        """
        client = boto3.client(
            "s3",
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
            region_name=os.getenv("REGION"),
        )

        return client
