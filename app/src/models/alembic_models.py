from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.orm import declarative_base
from sqlalchemy.sql import func

Base = declarative_base()
metadata = Base.metadata


class User(Base):
    __tablename__ = "users"

    u_id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String, unique=True, nullable=False)
    password = Column(String, nullable=False)


class Project(Base):
    __tablename__ = "projects"

    p_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    description = Column(Text)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime)
    created_by = Column(Integer, ForeignKey("users.u_id"))
    updated_by = Column(Integer, ForeignKey("users.u_id"))


class Document(Base):
    __tablename__ = "documents"

    id = Column(Integer, primary_key=True, autoincrement=True)
    path = Column(String, nullable=False)
    doc_name = Column(String, nullable=False)
    project_id = Column(Integer, ForeignKey("projects.p_id"), nullable=False)


class Logo(Base):
    __tablename__ = "logos"

    id = Column(Integer, primary_key=True, autoincrement=True)
    path = Column(String, nullable=False)
    logo_file_name = Column(String, nullable=False)
    project_id = Column(Integer, ForeignKey("projects.p_id"), nullable=False)


class UserAccess(Base):
    __tablename__ = "user_access"

    ua_id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey("users.u_id"), nullable=False)
    project_id = Column(Integer, ForeignKey("projects.p_id"), nullable=False)
    role = Column(String, nullable=False)
