from fastapi import Body
from pydantic import BaseModel


class ProjectCreateIn(BaseModel):
    """
    Input scheme for create_project() function imposing restriction
    on name and description length:

    name: str (length <= 100)
    description: str (length <= 2000)
    """

    name: str = Body(max_length=100, examples=["New project"])
    description: str = Body(
        default="", max_length=2000, examples=["Some valuable info about the project"]
    )


class ProjectCreateOut(BaseModel):
    """
    Output scheme for create_project() function
    """

    project_id: int


class ProjectUpdateIn(BaseModel):
    """
    Input scheme for update_project_info() function imposing restriction
    on name and description length:

    name: str (length <= 100)
    description: str (length <= 2000)
    """

    name: str = Body(default="", max_length=100, examples=["Updated project name"])
    description: str = Body(
        default="",
        max_length=2000,
        examples=["Updated valuable info about the project"],
    )


class ProjectUpdateOut(BaseModel):
    """
    Output scheme for update_project_info() function
    """

    project_id: int
