from fastapi import Body
from pydantic import BaseModel, EmailStr


class UserCreateBase(BaseModel):
    """
    Base for UserCreateIn and UserCreateOut classes
    imposing restriction on login format:

    login: EmailStr (should be an email)
    """

    login: EmailStr = Body(max_length=50)


class UserCreateIn(UserCreateBase):
    """
    Input scheme for create_user() function imposing restriction
    on password, and repeat_password:

    password: str (length >= 8 and <=20)
    repeat_password: str (length >= 8 and <=20)
    """

    password: str = Body(min_length=8, max_length=20)
    repeat_password: str = Body(min_length=8, max_length=20)


class UserCreateOut(UserCreateBase):
    """
    Output scheme for create_user() function
    """

    pass


class UserLoginIn(BaseModel):
    """
    Input scheme for login_user() function imposing restriction
    on login and password:

    login: EmailStr (should be an email)
    password: str (length >= 8 and <=20)
    """

    login: EmailStr = Body(max_length=50)
    password: str = Body(min_length=8, max_length=20)


class UserLoginOut(BaseModel):
    """
    Output scheme for login_user() function
    """

    access_token: str
