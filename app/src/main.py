import datetime
import os
from enum import Enum
from typing import List

import jwt
from fastapi import Depends, FastAPI, HTTPException, Request, UploadFile, status
from jwt import ExpiredSignatureError
from pydantic import EmailStr
from sqlalchemy.orm import sessionmaker

from .models.alembic_models import Document, Logo, Project, User, UserAccess
from .models.project import (
    ProjectCreateIn,
    ProjectCreateOut,
    ProjectUpdateIn,
    ProjectUpdateOut,
)
from .models.user import UserCreateIn, UserCreateOut, UserLoginIn, UserLoginOut
from .services.db_connection_services import DBConnectionServices
from .services.document_services import DocumentServices
from .services.jwt_services import JWTServices
from .services.logger import logger
from .services.logo_services import LogoServices
from .services.project_services import ProjectServices
from .services.s3_connection_services import S3ConnectionServices
from .services.user_services import UserServices

app = FastAPI()


class Roles(Enum):
    OWNER = "owner"
    EDITOR = "editor"


async def rds_session():
    """
    Provides a database session for interacting with the RDS database.
    """
    db_connection = DBConnectionServices()

    if os.getenv("AWS_DB_HOST_NAME") and os.getenv("DOCKER_DB_HOST_NAME"):
        logger.warning(
            msg="Multiple environmental variable '*_DB_HOST_NAME' were passed."
        )
        logger.warning(msg="'AWS_DB_HOST_NAME' will be used by default.")
    if os.getenv("AWS_DB_HOST_NAME"):
        Session = sessionmaker(bind=db_connection.get_rds_engine())
        logger.info(msg="AWS RDS engine was created.")
    elif os.getenv("DOCKER_DB_HOST_NAME"):
        Session = sessionmaker(bind=db_connection.get_local_engine())
    else:
        logger.error(msg="Environmental variable '*_DB_HOST_NAME' was not passed.")

    session = Session()
    try:
        yield session
    finally:
        session.close()


@app.middleware("http")
async def resolving_permissions(request: Request, call_next):
    """
    Middleware function for resolving permissions based on JWT authentication.

    This middleware checks if the incoming request requires JWT authentication
    and verifies the JWT token. If the request path does not require JWT
    authentication, it proceeds to the endpoint without any further action.

    :request: Request - incoming request object;
    :call_next: callable - function to call endpoint in the request chain;
    :return: the HTTP response returned by the next middleware or endpoint.
    """
    routes_w_jwt = ["/projects", "/project", "/document"]
    jwt_policy = False

    for rout in routes_w_jwt:
        if rout == request.url.path or rout in request.url.path:
            jwt_policy = True

    if jwt_policy:
        auth_header_content = request.headers.get("Authorization")

        if auth_header_content is None or not auth_header_content.startswith("Bearer "):
            logger.error(msg="JWT was not passed or the wrong prefix was used.")
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="JWT was not passed or the wrong prefix was used.",
            )

        jwt_token = request.headers.get("Authorization")[len("Bearer ") :]
        db_connection = DBConnectionServices()
        Session = sessionmaker(bind=db_connection.get_rds_engine())
        session = Session()
        try:
            jwt_content = jwt.decode(
                jwt_token,
                os.getenv("JWT_SECRET"),
                algorithms=[os.getenv("JWT_ALGORITHM")],
            )

            user = session.query(User).filter_by(login=jwt_content.get("login")).first()

            if not user.login:
                logger.error(msg="Access denied")
                logger.error(
                    msg=f"The user with login {user.login} is not an authorized user"
                )
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail=f"The user with login {user.login} is not an authorized user",
                )

            logger.info(msg="Access provided")

        except ExpiredSignatureError:
            logger.error(msg="JWT has expired")

        except BaseException:
            logger.error(msg="Incorrect JWT was passed")
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Incorrect JWT was passed",
            )

    response = await call_next(request)

    return response


@app.get(path="/")
async def root():
    """
    This is the test function for revision 3.
    """
    return {"response": "Projects Manager"}


@app.get(path="/projects", status_code=status.HTTP_200_OK)
async def get_projects_ls(request: Request, session=Depends(rds_session)):
    """
    Return the json storing the list which contains the info about all projects
    to which the user have access.
    Authorization is ensured by JWT.

    \n:request: Request - incoming request object;
    \n:session: Session - The RDS session;
    \n:return: json.
    """

    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    available_projects = (
        session.query(UserAccess, Project)
        .join(Project)
        .filter(UserAccess.user_id == user.u_id)
        .all()
    )
    projects_table_content = []

    for project in available_projects:
        projects_table_content.append(project[1])

    return projects_table_content


@app.post(
    path="/projects",
    response_model=ProjectCreateOut,
    status_code=status.HTTP_201_CREATED,
)
async def create_project(
    request: Request, project_info: ProjectCreateIn, session=Depends(rds_session)
):
    """
    Provide the creation of the new project by authorized user.
    Authorization is ensured by JWT.

    \n:project_info: ProjectIn - json storing name and description;
    \n:request: Request - incoming request object;
    \n:session: Session - The RDS session;
    \n:return: ProjectOut - json storing the response or error message.
    """

    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    owner = session.query(User).filter_by(login=jwt_content.get("login")).first()
    current_time = datetime.datetime.now()

    new_project = Project(
        name=project_info.name,
        description=project_info.description,
        created_by=owner.u_id,
        created_at=current_time,
    )
    session.add(new_project)
    session.commit()

    created_project = (
        session.query(Project)
        .filter_by(name=project_info.name, created_at=current_time)
        .first()
    )

    new_user_access = UserAccess(
        user_id=owner.u_id, project_id=created_project.p_id, role=Roles.OWNER.value
    )
    session.add(new_user_access)
    session.commit()

    project_info_out = ProjectCreateOut(project_id=created_project.p_id)

    return project_info_out


@app.get(path="/project/{project_id}/info", status_code=status.HTTP_200_OK)
async def get_certain_project_info(
    project_id: int,
    request: Request,
    session=Depends(rds_session),
):
    """
    Takes the project ID returns the whole project info if it exists
    and user has access to such info. Authorization is ensured by JWT.

    \n:project_id: int - the ID of the project;
    \n:request: Request - incoming request object;
    \n:session: Session - The RDS session;
    \n:return: json storing the response or error message.
    """

    project = ProjectServices.get_project(project_id=project_id)
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()

    ProjectServices.check_user_authority(
        user=user,
        project_id=project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )
    users_with_access = session.query(UserAccess).filter_by(project_id=project_id).all()
    access = False

    for user_with_access in users_with_access:
        if user_with_access.user_id == user.u_id:
            access = True

    if not access:
        logger.error(msg="Access denied. User dos not have access to this project")
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Access denied. User dos not have access to this project",
        )

    return project


@app.put(
    path="/project/{project_id}/info",
    response_model=ProjectUpdateOut,
    status_code=status.HTTP_200_OK,
)
async def update_project_info(
    project_id: int,
    request: Request,
    project_info: ProjectUpdateIn,
    session=Depends(rds_session),
):
    """
    Updates the name or description of the project with given ID if
    new project details were passed as an argument by authorized user
    with appropriate permissions. Authorization is ensured by JWT.

    \n:project_id: int - the ID of the project;
    \n:project_info: ProjectUpdateIn - new information about the project.
    \n:request: Request - incoming request object;
    \n:session: Session - The RDS session;
    \n:return: ProjectUpdateOut - json storing the response or error message.
    """

    ProjectServices.get_project(project_id=project_id)
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )

    current_time = datetime.datetime.now()
    project_info_dict = project_info.dict()
    project_info_dict.update({"updated_by": user.u_id})
    project_info_dict.update({"updated_at": current_time})

    for key, value in project_info_dict.items():
        if value:
            session.query(Project).filter(Project.p_id == project_id).update(
                {f"{key}": value}
            )

    session.commit()
    logger.info(msg=f"The project: with ID: {project_id} has been updated")

    project_info_out = ProjectUpdateOut(project_id=project_id)

    return project_info_out


@app.delete(path="/project/{project_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_project(
    project_id: int,
    request: Request,
    session=Depends(rds_session),
):
    """
    Delete the project with given ID and all related files and record in other tables
    if such project exists and user is authorized for this action
    (is admin of this project).

    \n:project_id: int - the ID of the project;
    \n:request: Request - incoming request object;
    \n:session: Session - The RDS session.
    """
    ProjectServices.get_project(project_id=project_id)
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user, project_id=project_id, authorized_roles=[Roles.OWNER.value]
    )

    s3 = S3ConnectionServices.get_s3_client()
    related_documents = session.query(Document).filter_by(project_id=project_id).all()

    if related_documents:
        for document in related_documents:
            path = f"documents/{project_id}/{document.doc_name}"
            s3.delete_object(Bucket=os.getenv("DOCS_BUCKET"), Key=path)
            session.query(Document).filter(Document.id == document.id).delete()
            session.commit()
            logger.info(msg=f"Document {document.doc_name} has been deleted.")

    related_logo = session.query(Logo).filter_by(project_id=project_id).first()

    if related_logo:
        s3.delete_object(Bucket=os.getenv("LOGO_BUCKET"), Key=related_logo.path)
        s3.delete_object(
            Bucket=os.getenv("PROCESSED_LOGO_BUCKET"), Key=related_logo.path
        )
        session.query(Logo).filter(Logo.project_id == project_id).delete()
        session.commit()
        logger.info(msg=f"Logo {related_logo.logo_file_name} has been deleted.")

    session.query(UserAccess).filter(
        UserAccess.project_id == project_id,
        (UserAccess.role == Roles.OWNER.value)
        | (UserAccess.role == Roles.EDITOR.value),
    ).delete()

    session.query(Project).filter(Project.p_id == project_id).delete()
    session.commit()
    logger.info(msg=f"The project with ID {project_id} has been deleted.")


@app.post(
    path="/auth", response_model=UserCreateOut, status_code=status.HTTP_201_CREATED
)
async def create_user(user_info: UserCreateIn, session=Depends(rds_session)):
    """
    Create new user if the login and password fit all requirements
    and the user with the given login does not exist yet.

    \n:user_info: UserCreateIn - login and password of the new user;
    \n:session: Session - The RDS session;
    \n:return: UserCreateOut - login of the new user.
    """
    UserServices.check_password_match(user_info=user_info)

    # password complexity check
    # TODO implement password complexity validator

    UserServices.check_user_repetitiveness(session=session, user_info=user_info)

    hashed_password = UserServices.hash_password(password=user_info.password)
    new_user = User(login=user_info.login, password=hashed_password)
    session.add(new_user)
    session.commit()

    user_info_out = UserCreateOut(login=user_info.login)

    return user_info_out


@app.get(path="/users", status_code=status.HTTP_200_OK)
async def get_users_ls(session=Depends(rds_session)):
    """
    Return the json storing the list of existing users.

    \n:session: Session - The RDS session;
    \n:return: json.
    """

    users_table_content = []
    users = session.query(User).all()

    for user in users:
        current_user = UserCreateOut(login=user.login)
        users_table_content.append(current_user)

    return users_table_content


@app.post(path="/login", response_model=UserLoginOut, status_code=status.HTTP_200_OK)
async def login_user(user_info: UserLoginIn, session=Depends(rds_session)):
    """
    Authorize user and provide authorization via JWT.
    \n:return: UserLoginOut.
    """

    user = UserServices.get_user(session=session, user_login=user_info.login)
    UserServices.check_user_password(user=user, user_info=user_info)

    logger.info(msg=f"User with login: {user_info.login} has been authorized")
    encoded_jwt = JWTServices.encrypt_access_token(
        login=user_info.login, expiration_time=int(os.getenv("JWT_EXPIRATION_TIME"))
    )
    user_login_out = UserLoginOut(access_token=encoded_jwt)

    return user_login_out


@app.post(path="/project/{project_id}/invite", status_code=status.HTTP_200_OK)
async def invite_user_to_proj(
    project_id: int,
    invited_user_login: EmailStr,
    request: Request,
    session=Depends(rds_session),
):
    """
    Provide the access to the project with given ID for the invited user if:
    \n- the user provided JWT is owner of the project (authorized to invite new users);
    \n- the invited user login differ from the admin user login;
    \n- the invited user is authorized user (registered user with login and password)

    \n:project_id: int - the ID of the project;
    \n:invited_user_login: EmailStr - the login of the invited user;
    \n:request: Request - incoming request object;
    \n:session: Session - The RDS session.
    """

    ProjectServices.get_project(project_id=project_id)
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    owner = session.query(User).filter_by(login=jwt_content.get("login")).first()

    ProjectServices.compare_owner_invited_user(
        owner=owner, invited_user=invited_user_login
    )
    ProjectServices.check_user_authority(
        user=owner, project_id=project_id, authorized_roles=[Roles.OWNER.value]
    )
    invited_user = UserServices.get_user(session=session, user_login=invited_user_login)

    new_user_access = UserAccess(
        user_id=invited_user.u_id, project_id=project_id, role="editor"
    )
    session.add(new_user_access)
    session.commit()


@app.post(path="/project/{project_id}/documents", status_code=status.HTTP_201_CREATED)
async def upload_documents(
    project_id: int,
    documents: List[UploadFile],
    request: Request,
    session=Depends(rds_session),
):
    """
    Uploads a document to a project.

    \n:document: UploadFile - The document file to be uploaded;
    \n:project_id: int - The ID of the project to which the document will be uploaded;
    \n:session: Session - The RDS session.
    \n:request: Request - incoming request object;
    \n:return: dict - A dictionary containing the filename of the uploaded document.
    """
    allowed_formats = [
        "text/plain",
    ]
    DocumentServices.check_document_attachment(document=documents[0])
    ProjectServices.get_project(project_id=project_id)
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )
    files_status = {"Files status": {}}

    s3 = S3ConnectionServices.get_s3_client()
    for document in documents:
        file_type_allowed = DocumentServices.check_file_format(
            document, allowed_format=allowed_formats
        )
        if file_type_allowed:
            path = f"documents/{project_id}/{document.filename}"
            document_content = await document.read()

            s3.put_object(
                Body=document_content, Bucket=os.getenv("DOCS_BUCKET"), Key=path
            )

            new_document = Document(
                path=path, doc_name=document.filename, project_id=project_id
            )
            session.add(new_document)

            logger.info(msg=f"Document {document.filename} has been uploaded.")
            files_status["Files status"][document.filename] = "uploaded"

        if not file_type_allowed:
            logger.warning(msg=f"Document {document.filename} has not been uploaded.")
            logger.warning(
                msg="The format of this file is not allowed for project documents."
            )
            files_status["Files status"][document.filename] = "rejected"

    session.commit()
    return files_status


@app.get(path="/project/{project_id}/documents", status_code=status.HTTP_200_OK)
async def get_all_documents(
    project_id: int, request: Request, session=Depends(rds_session)
):
    """
    Fetches all documents associated with a specific project.

    \n:project_id: int - The ID of the project for which documents are to be retrieved;
    \n:request:  Request - The incoming request object containing headers;
    \n:session: Session - The RDS session.
    \n:returns: dict - A dictionary containing project ID, name, and a list of documents.
    """
    project = ProjectServices.get_project(project_id=project_id)
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )

    project_docs_to_get = session.query(Document).filter_by(project_id=project_id).all()

    if not project_docs_to_get:
        logger.error(msg="This project does not have any documents.")
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="This project does not have any documents.",
        )

    project_docs = {
        "Project ID": project.p_id,
        "Project name": project.name,
        "Project documents": [],
    }

    s3 = S3ConnectionServices.get_s3_client()
    for document in project_docs_to_get:
        document_url = s3.generate_presigned_url(
            ClientMethod="get_object",
            Params={"Bucket": os.getenv("DOCS_BUCKET"), "Key": document.path},
            ExpiresIn=300,
        )

        project_docs["Project documents"].append(
            {
                "Document ID": document.id,
                "Document name": document.doc_name,
                "Link": document_url,
            }
        )

    return project_docs


@app.get(path="/document/{document_id}", status_code=status.HTTP_200_OK)
async def get_certain_document(
    document_id: int, request: Request, session=Depends(rds_session)
):
    """
    Retrieves a specific document based on its ID.

    \n:document_id: int - The ID of the document to retrieve;
    \n:request: Request - The incoming request object containing headers;
    \n:session: Session - The RDS session.
    \n:return: dict - A dictionary containing the name and content
    of the requested document.
    """
    project_doc_to_get = session.query(Document).filter_by(id=document_id).first()

    if not project_doc_to_get:
        logger.error(msg="There is no document with such ID.")
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="There is no document with such ID.",
        )

    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_doc_to_get.project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )

    s3 = S3ConnectionServices.get_s3_client()

    document_url = s3.generate_presigned_url(
        ClientMethod="get_object",
        Params={"Bucket": os.getenv("DOCS_BUCKET"), "Key": project_doc_to_get.path},
        ExpiresIn=300,
    )

    document = {
        "Document name": project_doc_to_get.doc_name,
        "Link": document_url,
    }
    return document


@app.put(path="/document/{document_id}", status_code=status.HTTP_200_OK)
async def update_certain_document(
    document_id: int,
    request: Request,
    document: UploadFile,
    session=Depends(rds_session),
):
    """
    Update a specific document with the given ID.

    \n:document_id: int - The ID of the document to update.
    \n:request: Request - The incoming request object containing headers.
    \n:document: UploadFile - The document file to be uploaded.
    \n:return: dict - A dictionary indicating the success of the operation.
    """
    DocumentServices.check_document_attachment(document=document)
    project_doc_to_update = DocumentServices.check_doc_existence(
        session=session, document_id=document_id
    )
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_doc_to_update.project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )

    s3 = S3ConnectionServices.get_s3_client()
    path = (
        f"documents/{project_doc_to_update.project_id}/{project_doc_to_update.doc_name}"
    )
    document_content = await document.read()

    s3.put_object(Body=document_content, Bucket=os.getenv("DOCS_BUCKET"), Key=path)

    logger.info(msg=f"Document {project_doc_to_update.doc_name} has been uploaded.")

    return {"Updated file": project_doc_to_update.doc_name}


@app.delete(path="/document/{document_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_certain_document(
    document_id: int, request: Request, session=Depends(rds_session)
):
    """
    Delete a specific document from the S3 bucket and documents table.

    \n:document_id: int - The ID of the document to be deleted.
    \n:request: Request - The incoming request object containing headers.
    \n:session: Session - The RDS session.
    \n:return: dict - A dictionary indicating the success of the operation.
    """
    project_doc_to_delete = DocumentServices.check_doc_existence(
        session=session, document_id=document_id
    )
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_doc_to_delete.project_id,
        authorized_roles=[Roles.OWNER.value],
    )

    s3 = S3ConnectionServices.get_s3_client()
    path = (
        f"documents/{project_doc_to_delete.project_id}/{project_doc_to_delete.doc_name}"
    )
    s3.delete_object(Bucket=os.getenv("DOCS_BUCKET"), Key=path)
    session.query(Document).filter(Document.id == document_id).delete()
    session.commit()
    logger.info(msg=f"Document {project_doc_to_delete.doc_name} has been deleted.")

    return {"Deleted file": project_doc_to_delete.doc_name}


@app.get(path="/project/{project_id}/logo", status_code=status.HTTP_200_OK)
async def get_project_logo(project_id, request: Request, session=Depends(rds_session)):
    """
    Retrieve the logo of a project.

    \n:project_id: str - The unique identifier of the project;
    \n:request: Request - The incoming request object;
    \n:session: Session - The RDS session.
    \n:returns: dict - A dictionary containing the link to download the project logo.
    """
    ProjectServices.get_project(project_id=project_id)
    project_logo_to_get = session.query(Logo).filter_by(project_id=project_id).first()

    if not project_logo_to_get:
        logger.error(msg="This project does not have logo.")
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="This project does not have logo.",
        )

    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )

    s3 = S3ConnectionServices.get_s3_client()

    logo_url = s3.generate_presigned_url(
        ClientMethod="get_object",
        Params={
            "Bucket": os.getenv("PROCESSED_LOGO_BUCKET"),
            "Key": project_logo_to_get.path,
        },
        ExpiresIn=300,
    )

    return {f"The link to download logo {project_logo_to_get.logo_file_name}": logo_url}


@app.put(path="/project/{project_id}/logo", status_code=status.HTTP_201_CREATED)
async def upload_project_logo(
    project_id, logo: UploadFile, request: Request, session=Depends(rds_session)
):
    """
    Uploads a logo for a project.

    \n:project_id: int - The ID of the project for which the logo is being uploaded;
    \n:logo: UploadFile - The logo file to be uploaded;
    \n:request: Request - The HTTP request object;
    \n:session: Session - The RDS session.
    \n:return: dict - A dictionary indicating the success of the logo upload.
    """
    allowed_formats = ["image/jpeg", "image/png"]
    DocumentServices.check_document_attachment(document=logo)
    file_type_allowed = DocumentServices.check_file_format(
        logo, allowed_format=allowed_formats
    )

    if not file_type_allowed:
        logger.warning(msg="The format of uploaded file is not appropriate for logo.")
        logger.warning(msg="The logo file must possess .jpg or .png extension.")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The format of uploaded file is not appropriate for logo.",
        )

    ProjectServices.get_project(project_id=project_id)
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )
    project_logo = session.query(Logo).filter_by(project_id=project_id).first()
    logo_file_name = logo.filename

    if project_logo:
        logger.info(msg="This project already has logo.")
        logger.info(msg=f"The logo {project_logo.logo_file_name} will be updated.")
        logo_file_name = project_logo.logo_file_name

    logo_content = await logo.read()

    path = f"logos/{project_id}/{logo_file_name}"
    s3 = S3ConnectionServices.get_s3_client()
    s3.put_object(Body=logo_content, Bucket=os.getenv("LOGO_BUCKET"), Key=path)

    if not project_logo:
        new_logo = Logo(path=path, logo_file_name=logo.filename, project_id=project_id)
        session.add(new_logo)
        session.commit()

    logger.info(msg=f"Logo {logo_file_name} has been uploaded.")

    return {"Uploaded logo": logo_file_name}


@app.delete(path="/project/{project_id}/logo", status_code=status.HTTP_204_NO_CONTENT)
async def delete_project_logo(
    project_id: int, request: Request, session=Depends(rds_session)
):
    """
    Deletes the logo associated with a project.

    \n:project_id: int - The ID of the project whose logo is to be deleted;
    \n:request: Request - The HTTP request object;
    \n:session: Session - The RDS session.
    \n:return: dict - A dictionary indicating the success of the logo deletion.
    """
    project_logo_to_delete = LogoServices.check_logo_existence(
        session=session, project_id=project_id
    )
    access_token = request.headers.get("Authorization")
    jwt_content = JWTServices.decrypt_access_token(access_token=access_token)
    user = session.query(User).filter_by(login=jwt_content.get("login")).first()
    ProjectServices.check_user_authority(
        user=user,
        project_id=project_id,
        authorized_roles=[Roles.OWNER.value, Roles.EDITOR.value],
    )

    s3 = S3ConnectionServices.get_s3_client()
    s3.delete_object(Bucket=os.getenv("LOGO_BUCKET"), Key=project_logo_to_delete.path)
    s3.delete_object(
        Bucket=os.getenv("PROCESSED_LOGO_BUCKET"), Key=project_logo_to_delete.path
    )
    session.query(Logo).filter(Logo.project_id == project_id).delete()
    session.commit()
    logger.info(msg=f"Logo {project_logo_to_delete.logo_file_name} has been deleted.")

    return {"Deleted logo": project_logo_to_delete.logo_file_name}
